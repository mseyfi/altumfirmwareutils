# AltumFirmwareUtils
just create an interface function in hisilicon to use the current library 

**
int puttext_Altumview(VIDEO_FRAME_INFO_S* pstVFrameInfo, int width ,int height, char* text, POINT_S point,unsigned char fontSize)
{
    AltumPoint points;
    points.x = point.s32X;
    points.y = point.s32Y;
    HI_U8 *pData;
    HI_U32 u32Size;
    u32Size = pstVFrameInfo->stVFrame.u32Stride[0] * pstVFrameInfo->stVFrame.u32Height;
    pData = (HI_U8*) HI_MPI_SYS_Mmap(pstVFrameInfo->stVFrame.u32PhyAddr[0], u32Size);
    putText((void*)pData, text, width, height, points,fontSize);
    HI_MPI_SYS_Munmap((HI_VOID *)pData, u32Size);

return HI_SUCCESS;
}

**
