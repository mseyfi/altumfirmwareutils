/********************************************************************************
**    Copyright (c) Altumview Systems Inc. Canada, 2016-2018, All Rights Reserved
**
**    Common Utilities
**
**    Revision
**    - 2018 - Em Seyfi - Created
********************************************************************************/
#include "cvutil.h"
#include "CImg.h"

using namespace cimg_library;
using namespace std;


extern "C" void putText(void *pixels,char* text, unsigned width, unsigned height, int x, int y, unsigned char fontSize)
{
    unsigned char foreground_color = 155;
    unsigned char background_color = 90;

    CImgList<unsigned char> font_ = CImgList<unsigned char>::font(fontSize);

    CImg<unsigned char> CimgAltum1((unsigned char*)pixels,width,height,1,1,true);

    CimgAltum1.draw_text(x,y,text,&foreground_color,&background_color,1,font_);
}
