/********************************************************************************
**    Copyright (c) Altumview Systems Inc. Canada, 2016-2018, All Rights Reserved
**
**    Common Utilities
**
**    Revision
**    - 2018 - Em Seyfi - Created
********************************************************************************/


/*********************************************************************************************************************************

**    Sample code to interace the current library in hi3519

int puttext_Altumview(VIDEO_FRAME_INFO_S* pstVFrameInfo, int width ,int height, char* text, POINT_S point,unsigned char fontSize)
{
    AltumPoint points;
    points.x = point.s32X;
    points.y = point.s32Y;
    HI_U8 *pData;
    HI_U32 u32Size;
    u32Size = pstVFrameInfo->stVFrame.u32Stride[0] * pstVFrameInfo->stVFrame.u32Height;
    pData = (HI_U8*) HI_MPI_SYS_Mmap(pstVFrameInfo->stVFrame.u32PhyAddr[0], u32Size);
    putText((void*)pData, text, width, height, points,fontSize);
    HI_MPI_SYS_Munmap((HI_VOID *)pData, u32Size);

return HI_SUCCESS;
}

***************************************************************************************************************************************/

#ifndef CVUTIL_H
#define CVUTIL_H


#ifndef __cplusplus
#include <stdbool.h>
#endif

#ifdef __cplusplus
extern "C"
{
#endif

void putText(void *pixels,char* text, unsigned width, unsigned height, int x, int y, unsigned char fontSize);
#ifdef __cplusplus
} // extern "C"
#endif

#endif //CVUTIL_H
