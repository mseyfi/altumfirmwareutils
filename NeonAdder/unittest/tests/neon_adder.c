/*******************************************************************************
*    Copyright (c) Altumview Systems Inc. Canada, 2016-2018, All Rights Reserved
*
*    Unit Test
*
*    Revision
*    -31 May 2018 - Mehdi Seyfi - Created
*******************************************************************************/
#include "unitTests.h"
#include "utils.h"
#include <time.h>   
#include <stdlib.h>

static int* src1 = NULL;
static int* src2 = NULL;
static int* dstTest = NULL;
static int* dst = NULL;
static int size;


void setUp_adder(void)
{

	struct timeval time_now;
	gettimeofday(&time_now, NULL);

	srand (time_now.tv_usec);
	size 	= rand()%1000;
	src1 	= (int*)malloc(size*sizeof(int));
	src2 	= (int*)malloc(size*sizeof(int));
	dstTest = (int*)malloc(size*sizeof(int));
	dst  	= (int*)malloc(size*sizeof(int));

	int i;
	srand (time(NULL));
	for ( i = 0 ; i < size ; i++)
	{
		src1[i] = rand()%size;
		src2[i] = rand()%size;
		dstTest[i] = src1[i] + src2[i]+1;
	}
}

void tearDown_adder(void)
{

	free((void*)(src1));
	free((void*)(src2));
	free((void*)(dst));
	free((void*)(dstTest));


}


void test_neon_adder(void)
{
	char string[100];
	sprintf(string,"neon adder with size %d",size); 
	eltwiseAdd(src1, src2, dst, size);
	TEST_ASSERT_EQUAL_INT_ARRAY_MESSAGE(dstTest,dst ,size, string);
}



