/*******************************************************************************
*    Copyright (c) Altumview Systems Inc. Canada, 2016-2018, All Rights Reserved
*
*    Unit Test
*
*    Revision
*    -31 May 2018 - Mehdi Seyfi - Created
*******************************************************************************/
#ifndef UNIT_TESTS_H
#define UNIT_TESTS_H
#include "unity.h"

extern void test_neon_adder(void);
extern void setUp_adder(void);
extern void tearDown_adder(void);

#endif

