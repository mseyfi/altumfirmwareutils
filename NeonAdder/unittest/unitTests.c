/*******************************************************************************
*    Copyright (c) Altumview Systems Inc. Canada, 2016-2018, All Rights Reserved
*
*    Unit Test
*
*    Revision
*    -31 May 2018 - Mehdi Seyfi - Created
*******************************************************************************/


#define RUN_TEST(TestFunc, TestLineNum, setUpFunc,tearDownFunc) \
{ \
    Unity.CurrentTestName = #TestFunc; \
    Unity.CurrentTestLineNumber = TestLineNum; \
    Unity.NumberOfTests++;\
    Unity.TestFile = #TestFunc;\
    if (TEST_PROTECT()) \
        { \
            setUpFunc(); \
            TestFunc(); \
        } \
        if (TEST_PROTECT()) \
            { \
                tearDownFunc(); \
            } \
            UnityConcludeTest(); \
        }



#include <setjmp.h>
#include <stdio.h>
#include "unitTests.h"



/*=======Suite Setup=====*/
#if defined(UNITY_WEAK_ATTRIBUTE) || defined(UNITY_WEAK_PRAGMA)
        void suiteSetUp(){};
#endif
        static void suite_setup(void)
        {
#if defined(UNITY_WEAK_ATTRIBUTE) || defined(UNITY_WEAK_PRAGMA)
          suiteSetUp();
#endif
      }


      static int suite_teardown(int num_failures)
      {
#if defined(UNITY_WEAK_ATTRIBUTE) || defined(UNITY_WEAK_PRAGMA)
          return suiteTearDown(num_failures);
#else
          return num_failures;
#endif
      }


      static int run_unit_tests()
      {           
         int i;  
         for (i = 0 ; i < 20 ; i++)
            RUN_TEST(test_neon_adder, 61,setUp_adder,tearDown_adder);
        
        return 0;
    }

    int main(int argc, char* argv[])
    {

        suite_setup();
        UNITY_BEGIN();
        printf("\n\n*******************************\n");
        printf("******Starting Unit Tests******\n");
        printf("*******************************\n\n");
        run_unit_tests();

        printf("\n\n*******************************\n");
        printf("******Unit Tests Finished******\n");
        printf("*******************************\n\n");

        return suite_teardown(UNITY_END());
    }






