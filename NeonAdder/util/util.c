/*******************************************************************************
*    Copyright (c) Altumview Systems Inc. Canada, 2016-2018, All Rights Reserved
*
*    Unit Test
*
*    Revision
*    -31 May 2018 - Mehdi Seyfi - Created
*******************************************************************************/

#ifdef __ARM_NEON__
#include<arm_neon.h>
#endif
#include "utils.h"

void eltwiseAdd(int *src1, int *src2, int*dst, int size)
{

int i,j;
for(i = 0; i <= size - 4; i +=4)
{
 __builtin_prefetch(src1 + i + 4);
 __builtin_prefetch(src2 + i + 4);

int32x4_t  t1 = vld1q_s32(src1 + i);
int32x4_t  t2 = vld1q_s32(src2 + i);
int32x4_t  r0 = vaddq_s32(t1, t2);
vst1q_s32(dst + i, r0);
}
for (j = i ; j < size; j++){dst[j] = src1[j]+src2[j];}
}









